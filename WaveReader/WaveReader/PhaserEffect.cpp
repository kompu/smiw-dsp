#include "PhaserEffect.h"

#define F_PI 3.14159265358979323846

PhaserEffect::PhaserEffect() : _fb(0.7f) , _lfoPhase(0.0f) , _depth(1.0f) , _zm1(0.0f)
{
}

void PhaserEffect::onPrepare()
{
	Range(400.f, 1600.f);
	Rate(0.5f);
}


PhaserEffect::~PhaserEffect()
{
}


void PhaserEffect::process(int left, int right)
{
	int sample = (left + right) / 2;
	//int sample = left;

	float fSample = Update((float)sample);
	sample = (int)(fSample / 2);

	writeToBuffer(sample, sample);
}

void PhaserEffect::Range(float fMin, float fMax)
{
	_dmin = fMin / (sampleRate / 2.f);
	_dmax = fMax / (sampleRate / 2.f);
}

void PhaserEffect::Rate(float rate)
{
	_lfoInc = 2.f * F_PI * (rate / sampleRate);
}

void PhaserEffect::Feedback(float fb)
{
	_fb = fb;
}

void PhaserEffect::Depth(float depth)
{
	_depth = depth;
}

float PhaserEffect::Update(float inSamp)
{
	//calculate and update phaser sweep lfo...
	float d = _dmin + (_dmax - _dmin) * ((sin(_lfoPhase) + 1.f) / 2.f);
	_lfoPhase += _lfoInc;
	if (_lfoPhase >= F_PI * 2.f)
		_lfoPhase -= F_PI * 2.f;

	//update filter coeffs
	for (int i = 0; i < ALL_PASS_COUNT; i++)
		_alps[i].Delay(d);

	//calculate output
	float y = inSamp + _zm1 * _fb;
	for (int i = ALL_PASS_COUNT - 1; i >= 0; --i)
	{
		y = _alps[i].Update(y);
	}
	_zm1 = y;

	return inSamp + y * _depth;
}
