#pragma once
#include "WaveReader.h"

class SecondPhaserEffect : public WaveReader
{
public:
	SecondPhaserEffect();
	~SecondPhaserEffect();

	void onPrepare() override;
	void process(int left, int right) override;
};
