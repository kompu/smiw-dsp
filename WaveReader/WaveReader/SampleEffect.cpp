#include "SampleEffect.h"


SampleEffect::SampleEffect()
{
}


SampleEffect::~SampleEffect()
{
}

void SampleEffect::process(int left, int right)
{
	int sample = (left + right) / 2;

	writeToBuffer(sample, sample);
}
