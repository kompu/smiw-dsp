#pragma once
#include "WaveReader.h"
#include "AllpassDelay.h"

#define ALL_PASS_COUNT 4

class PhaserEffect : public WaveReader
{
private:
	AllpassDelay _alps[ALL_PASS_COUNT];

	float _dmin, _dmax; //range
	float _fb; //feedback
	float _lfoPhase;
	float _lfoInc;
	float _depth;

	float _zm1;

	void Range(float fMin, float fMax);
	void Rate(float rate);
	void Feedback(float fb);
	void Depth(float depth);
	float Update(float inSamp);
public:
	PhaserEffect();
	~PhaserEffect();

	void onPrepare() override;
	void process(int left, int right) override;
};

