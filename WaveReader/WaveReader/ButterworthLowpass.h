#pragma once
#define _USE_MATH_DEFINES
#include <cmath>
#include "WaveReader.h"

class ButterworthLowpass : public WaveReader
{
private:
	int n;
	double s;
	double f;
	double a;
	double a2;
	double r;
	std::vector<double> A;
	std::vector<double> d1;
	std::vector<double> d2;
	std::vector<double> w0;
	std::vector<double> w1;
	std::vector<double> w2;
public:
	ButterworthLowpass();
	~ButterworthLowpass();

	void onPrepare() override;
	void process(int left, int right) override;
};

