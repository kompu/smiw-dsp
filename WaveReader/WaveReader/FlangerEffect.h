#pragma once
#include "WaveReader.h"
#include "Delay.h"

class FlangerEffect : public WaveReader
{
public:
	FlangerEffect();
	~FlangerEffect();

	void onPrepare() override;
	void process(int left, int right) override;
};

