#pragma once
#include "WaveReader.h"

class DowngradeEffect : public WaveReader
{
private:
	const int DOWNGRADE_LEVEL = 8;
	int buffor;
	int bufforPosition;
public:
	DowngradeEffect();
	~DowngradeEffect();

	void process(int left, int right) override;
};

