#include "ButterworthLowpass.h"

ButterworthLowpass::ButterworthLowpass()
{
}

ButterworthLowpass::~ButterworthLowpass()
{
}

void ButterworthLowpass::onPrepare()
{
	n = 4;
	s = sampleRate;
	f = 1000.0;
	a = tan(M_PI * f / s);
	a2 = a*a;
	r;
	A = std::vector<double>(n);
	d1 = std::vector<double>(n);
	d2 = std::vector<double>(n);
	w0 = std::vector<double>(n);
	w1 = std::vector<double>(n);
	w2 = std::vector<double>(n);

	for (int i = 0; i < n; ++i) 
	{
		r = sin(M_PI * (2.0 * i + 1.0) / (4.0 * n));
		s = a2 + 2.0 * a * r + 1.0;
		A[i] = a2 / s;
		d1[i] = 2.0 * (1 - a2) / s;
		d2[i] = -(a2 - 2.0 * a * r + 1.0) / s;
	}
}

void ButterworthLowpass::process(int left, int right)
{
	int sample = (left + right) / 2;

	for (int i = 0; i < n; ++i)
	{
		w0[i] = d1[i] * w1[i] + d2[i] * w2[i] + sample;
		sample = A[i] * (w0[i] + 2.0 * w1[i] + w2[i]);
		w2[i] = w1[i];
		w1[i] = w0[i];
	}

	writeToBuffer(sample, sample);
}
