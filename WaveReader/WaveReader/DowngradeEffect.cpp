#include "DowngradeEffect.h"


DowngradeEffect::DowngradeEffect()
{
	bufforPosition = DOWNGRADE_LEVEL;
}


DowngradeEffect::~DowngradeEffect()
{
}

void DowngradeEffect::process(int left, int right)
{
	int sample = (left + right) / 2;

	if (bufforPosition++ >= DOWNGRADE_LEVEL)
	{
		bufforPosition = 0;
		buffor = sample;
	}

	writeToBuffer(buffor, buffor);
}
