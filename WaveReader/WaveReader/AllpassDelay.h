#pragma once
class AllpassDelay
{
private:
	float _a1, _zm1;
public:
	AllpassDelay();
	~AllpassDelay();

	void Delay(float delay);
	float Update(float inSamp);
};

