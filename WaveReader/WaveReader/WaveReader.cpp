#include "WaveReader.h"


WaveReader::WaveReader()
{
	inputPosition = outputPosition = 0;
}


WaveReader::~WaveReader()
{
}

void WaveReader::read(std::string path)
{
	std::cout << "Reading file..." << std::endl;

	std::ifstream input(path, std::ios::binary);
	inBuffer = std::vector<char>((std::istreambuf_iterator<char>(input)), (std::istreambuf_iterator<char>()));
	outBuffer = std::vector<char>(inBuffer.size());

	std::cout << "Processing file..." << std::endl;

	chunkId = read32u();
	chunkSize = read32u();
	format = read32u();
	subchunk1id = read32u();
	subchunk1size = read32u();
	audioFormat = read16u();
	numChannels = read16u();
	sampleRate = read32u();
	byteRate = read32u();
	blockAlign = read16u();
	bitsPerSample = read16u();

	write32(chunkId);
	write32(chunkSize);
	write32(format);
	write32(subchunk1id);
	write32(subchunk1size);
	write16(audioFormat);
	write16(numChannels);
	write32(sampleRate);
	write32(byteRate);
	write16(blockAlign);
	write16(bitsPerSample);

	while (inputPosition < inBuffer.size())
	{
		write8(read8u());
		if (inBuffer[inputPosition] == 'a' && inBuffer[inputPosition - 1] == 't' && inBuffer[inputPosition - 2] == 'a' && inBuffer[inputPosition - 3] == 'd')
		{
			subchunk2size = read32u();
			write32(subchunk2size);
			write8(read8u());
			break;
		}
	}

	onPrepare();

	while (!isInputEnd())
	{
		int left = read16();
		int right = read16();
		process(left, right);
	}
}

void WaveReader::write(std::string path)
{
	onWrite();

	std::cout << "Writing file..." << std::endl;

	std::ofstream output(path, std::ios::binary);
	std::copy(outBuffer.begin(), outBuffer.end(), std::ostreambuf_iterator<char>(output));

	std::cout << "Completed!" << std::endl;
}

void WaveReader::onPrepare() { }

void WaveReader::onWrite() { }

bool WaveReader::isInputEnd()
{
	return inputPosition >= inBuffer.size();
}

bool WaveReader::isOutputEnd()
{
	return outputPosition >= outBuffer.size();
}

char WaveReader::read8()
{
	if (inputPosition >= inBuffer.size())
	{
		std::cout << "Something went wrong!" << std::endl;
		inputPosition = inBuffer.size();
		return 0;
	}
	char byte0 = inBuffer[inputPosition++];
	return byte0;
}

int WaveReader::read16()
{
	if (inputPosition + 1 >= inBuffer.size())
	{
		std::cout << "Something went wrong!" << std::endl;
		inputPosition = inBuffer.size();
		return 0;
	}
	char byte0 = inBuffer[inputPosition++];
	char byte1 = inBuffer[inputPosition++];
	return byte0 + (byte1 << 8);
}

int WaveReader::read32()
{
	if (inputPosition + 3 >= inBuffer.size())
	{
		std::cout << "Something went wrong!" << std::endl;
		inputPosition = inBuffer.size();
		return 0;
	}
	char byte0 = inBuffer[inputPosition++];
	char byte1 = inBuffer[inputPosition++];
	char byte2 = inBuffer[inputPosition++];
	char byte3 = inBuffer[inputPosition++];
	return byte0 + (byte1 << 8) + (byte2 << 16) + (byte3 << 24);
}

unsigned char WaveReader::read8u()
{
	if (inputPosition >= inBuffer.size())
	{
		std::cout << "Something went wrong!" << std::endl;
		inputPosition = inBuffer.size();
		return 0;
	}
	unsigned char byte0 = inBuffer[inputPosition++];
	return byte0;
}

unsigned int WaveReader::read16u()
{
	if (inputPosition + 1 >= inBuffer.size())
	{
		std::cout << "Something went wrong!" << std::endl;
		inputPosition = inBuffer.size();
		return 0;
	}
	unsigned char byte0 = inBuffer[inputPosition++];
	unsigned char byte1 = inBuffer[inputPosition++];
	return byte0 + (byte1 << 8);
}

unsigned int WaveReader::read32u()
{
	if (inputPosition + 3 >= inBuffer.size())
	{
		std::cout << "Something went wrong!" << std::endl;
		inputPosition = inBuffer.size();
		return 0;
	}
	unsigned char byte0 = inBuffer[inputPosition++];
	unsigned char byte1 = inBuffer[inputPosition++];
	unsigned char byte2 = inBuffer[inputPosition++];
	unsigned char byte3 = inBuffer[inputPosition++];
	return byte0 + (byte1 << 8) + (byte2 << 16) + (byte3 << 24);
}

void WaveReader::write8(char byte)
{
	if (outputPosition >= outBuffer.size())
	{
		std::cout << "Something went wrong!" << std::endl;
		outputPosition = outBuffer.size() - 1;
	}
	outBuffer[outputPosition++] = byte;
}

void WaveReader::write16(int word)
{
	if (outputPosition + 1 >= outBuffer.size())
	{
		std::cout << "Something went wrong!" << std::endl;
		outputPosition = outBuffer.size() - 2;
	}
	outBuffer[outputPosition++] = word;
	outBuffer[outputPosition++] = (word >> 8);
}

void WaveReader::write32(int dword)
{
	if (outputPosition + 3 >= outBuffer.size())
	{
		std::cout << "Something went wrong!" << std::endl;
		outputPosition = outBuffer.size() - 4;
	}
	outBuffer[outputPosition++] = dword;
	outBuffer[outputPosition++] = (dword >> 8);
	outBuffer[outputPosition++] = (dword >> 16);
	outBuffer[outputPosition++] = (dword >> 24);
}

void WaveReader::writeToBuffer(int left, int right)
{
	write16(left);
	write16(right);
}