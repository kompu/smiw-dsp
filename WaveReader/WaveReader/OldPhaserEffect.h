#pragma once
#include "WaveReader.h"

class OldPhaserEffect : public WaveReader
{
public:
	OldPhaserEffect();
	~OldPhaserEffect();

	void onPrepare() override;
	void process(int left, int right) override;
};

