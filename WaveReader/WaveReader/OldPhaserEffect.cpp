#include "OldPhaserEffect.h"

#define M_PI 3.141592653589793
#define STAGES_COUNT 12
#define SAMPLE_RATE sampleRate
#define RANGE_MIN 400
#define RANGE_MAX 1600
#define FEEDBACK 0.9
#define DEPTH 1.0
#define SWEEP_RATE 0.125

double dmin;
double dmax;
double lfoInc;
double lfoPhase;
double last;

struct AllpassFilter
{
	double out;
};

struct AllpassFilter filters[STAGES_COUNT];

void init(struct AllpassFilter* filter)
{
	filter->out = 0.0;
}

double filter(struct AllpassFilter* filter, double a, double sample)
{
	//filter->out = a * (sample + filter->out) - filter->in;
	//filter->in = sample;
	//return filter->out;

	double y = sample * -a + filter->out;
	filter->out = y * a + sample;
	return y;
}

void phaserInit(int sampleRate)
{
	dmin = RANGE_MIN / (SAMPLE_RATE / 2.f);
	dmax = RANGE_MAX / (SAMPLE_RATE / 2.f);
	lfoInc = 2.0 * M_PI * (SWEEP_RATE / SAMPLE_RATE);
	lfoPhase = 0.0;
	for (int i = 0; i < STAGES_COUNT; ++i)
	{
		init(&filters[i]);
	}
}

double phaserProcess(double sample)
{
	float d = dmin + (dmax - dmin) * ((sin(lfoPhase) + 1.0) / 2.0);
	d = (1 - d) / (1 + d);
	lfoPhase += lfoInc;
	if (lfoPhase >= M_PI * 2.f) 
		lfoPhase -= M_PI * 2.f;

	double val = sample + last * FEEDBACK;
	for (int i = 0; i < STAGES_COUNT; ++i)
		val = filter(&filters[i], d, val);
	last = val;

	return sample + val * DEPTH;
}


OldPhaserEffect::OldPhaserEffect()
{
}


OldPhaserEffect::~OldPhaserEffect()
{
}

void OldPhaserEffect::onPrepare()
{
	phaserInit(sampleRate);
}

void OldPhaserEffect::process(int left, int right)
{
	int sample = (left + right) / 2;
	//int sample = left;

	sample = (int)(phaserProcess(sample) / 4.0);

	writeToBuffer(sample, sample);
}
