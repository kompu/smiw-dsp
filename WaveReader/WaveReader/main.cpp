#include <iostream>
#include "SampleEffect.h"
#include "PitchShift.h"
#include "ButterworthLowpass.h"
#include "DowngradeEffect.h"
#include "FlangerEffect.h"
#include "PhaserEffect.h"
#include "SecondPhaserEffect.h"
#include "OldPhaserEffect.h"

using namespace std;

int main()
{
	//SampleEffect waveReader;
	//PitchShift waveReader;
	//ButterworthLowpass waveReader;
	//DowngradeEffect waveReader;
	//FlangerEffect waveReader;
	//PhaserEffect waveReader;
	//SecondPhaserEffect waveReader;
	OldPhaserEffect waveReader;
	waveReader.read("E:\\fernando.wav");
	//waveReader.read("E:\\led.wav");
	waveReader.write("E:\\test.wav");
	return 0;
}