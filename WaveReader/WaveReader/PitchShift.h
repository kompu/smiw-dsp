#pragma once
#define _USE_MATH_DEFINES
#include <cmath>
#include <vector>
#include "WaveReader.h"

class PitchShift : public WaveReader
{
private:
	std::vector<float> floatBuffer;
public:
	PitchShift();
	~PitchShift();

	void process(int left, int right) override;
	void onWrite();
};

