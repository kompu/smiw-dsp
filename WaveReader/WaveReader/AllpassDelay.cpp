#include "AllpassDelay.h"


AllpassDelay::AllpassDelay() : _a1(0.f), _zm1(0.f)
{
}


AllpassDelay::~AllpassDelay()
{
}


void AllpassDelay::Delay(float delay)
{
	_a1 = (1.f - delay) / (1.f + delay);
}

float AllpassDelay::Update(float inSamp)
{
	float y = inSamp * -_a1 + _zm1;
	_zm1 = y * _a1 + inSamp;

	return y;
}
