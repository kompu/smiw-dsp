#pragma once
#include <string>
#include <vector>
#include <fstream>
#include <iostream>

class WaveReader
{
protected:
	unsigned int chunkId;
	unsigned int chunkSize;
	unsigned int format;
	unsigned int subchunk1id;
	unsigned int subchunk1size;
	unsigned int audioFormat;
	unsigned int numChannels;
	unsigned int sampleRate;
	unsigned int byteRate;
	unsigned int blockAlign;
	unsigned int bitsPerSample;
	unsigned int subchunk2size;

	unsigned int inputPosition;
	unsigned int outputPosition;
	std::vector<char> inBuffer;
	std::vector<char> outBuffer;

	char read8();
	int read16();
	int read32();
	unsigned char read8u();
	unsigned int read16u();
	unsigned int read32u();

	void write8(char byte);
	void write16(int word);
	void write32(int dword);

	bool isInputEnd();
	bool isOutputEnd();
	void writeToBuffer(int left, int right);
public:
	WaveReader();
	~WaveReader();

	void read(std::string path);
	void write(std::string path);
	virtual void onPrepare();
	virtual void process(int left, int right) = 0;
	virtual void onWrite();
};

