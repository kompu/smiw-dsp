#pragma once
#include "WaveReader.h"

class SampleEffect : public WaveReader
{
public:
	SampleEffect();
	~SampleEffect();

	void process(int left, int right) override;
};

