#include "PitchShift.h"

void ShortTimeFourierTransform(float* fftBuffer, long fftFrameSize, long sign)
{
	float wr, wi, arg, temp;
	float tr, ti, ur, ui;
	long i, bitm, j, le, le2, k;

	for (i = 2; i < 2 * fftFrameSize - 2; i += 2)
	{
		for (bitm = 2, j = 0; bitm < 2 * fftFrameSize; bitm <<= 1)
		{
			if ((i & bitm) != 0) j++;
			j <<= 1;
		}
		if (i < j)
		{
			temp = fftBuffer[i];
			fftBuffer[i] = fftBuffer[j];
			fftBuffer[j] = temp;
			temp = fftBuffer[i + 1];
			fftBuffer[i + 1] = fftBuffer[j + 1];
			fftBuffer[j + 1] = temp;
		}
	}
	long max = (long)(log2(fftFrameSize) + .5);
	for (k = 0, le = 2; k < max; k++)
	{
		le <<= 1;
		le2 = le >> 1;
		ur = 1.0F;
		ui = 0.0F;
		arg = (float)M_PI / (le2 >> 1);
		wr = cos(arg);
		wi = sign * sin(arg);
		for (j = 0; j < le2; j += 2)
		{

			for (i = j; i < 2 * fftFrameSize; i += le)
			{
				tr = fftBuffer[i + le2] * ur - fftBuffer[i + le2 + 1] * ui;
				ti = fftBuffer[i + le2] * ui + fftBuffer[i + le2 + 1] * ur;
				fftBuffer[i + le2] = fftBuffer[i] - tr;
				fftBuffer[i + le2 + 1] = fftBuffer[i + 1] - ti;
				fftBuffer[i] += tr;
				fftBuffer[i + 1] += ti;

			}
			tr = ur * wr - ui * wi;
			ui = ur * wi + ui * wr;
			ur = tr;
		}
	}
}

int const MAX_FRAME_LENGTH = 16000;
float gInFIFO[MAX_FRAME_LENGTH];
float gOutFIFO[MAX_FRAME_LENGTH];
float gFFTworksp[2 * MAX_FRAME_LENGTH];
float gLastPhase[MAX_FRAME_LENGTH / 2 + 1];
float gSumPhase[MAX_FRAME_LENGTH / 2 + 1];
float gOutputAccum[2 * MAX_FRAME_LENGTH];
float gAnaFreq[MAX_FRAME_LENGTH];
float gAnaMagn[MAX_FRAME_LENGTH];
float gSynFreq[MAX_FRAME_LENGTH];
float gSynMagn[MAX_FRAME_LENGTH];
long gRover, gInit;

std::vector<float> DoPitchShift(float pitchShift, long numSampsToProcess, long fftFrameSize, long osamp, float sampleRate, std::vector<float> indata)
{
	double magn, phase, tmp, window, real, imag;
	double freqPerBin, expct;
	long i, k, qpd, index, inFifoLatency, stepSize, fftFrameSize2;


	std::vector<float> outdata = indata;
	/* set up some handy variables */
	fftFrameSize2 = fftFrameSize / 2;
	stepSize = fftFrameSize / osamp;
	freqPerBin = sampleRate / (double)fftFrameSize;
	expct = 2.0 * M_PI * (double)stepSize / (double)fftFrameSize;
	inFifoLatency = fftFrameSize - stepSize;
	if (gRover == 0) gRover = inFifoLatency;


	/* main processing loop */
	for (i = 0; i < numSampsToProcess; i++)
	{

		/* As long as we have not yet collected enough data just read in */
		gInFIFO[gRover] = indata[i];
		outdata[i] = gOutFIFO[gRover - inFifoLatency];
		gRover++;

		/* now we have enough data for processing */
		if (gRover >= fftFrameSize)
		{
			gRover = inFifoLatency;

			/* do windowing and re,im interleave */
			for (k = 0; k < fftFrameSize; k++)
			{
				window = -.5 * cos(2.0 * M_PI * (double)k / (double)fftFrameSize) + .5;
				gFFTworksp[2 * k] = (float)(gInFIFO[k] * window);
				gFFTworksp[2 * k + 1] = 0.0F;
			}


			/* ***************** ANALYSIS ******************* */
			/* do transform */
			ShortTimeFourierTransform(gFFTworksp, fftFrameSize, -1);

			/* this is the analysis step */
			for (k = 0; k <= fftFrameSize2; k++)
			{

				/* de-interlace FFT buffer */
				real = gFFTworksp[2 * k];
				imag = gFFTworksp[2 * k + 1];

				/* compute magnitude and phase */
				magn = 2.0 * sqrt(real * real + imag * imag);
				phase = atan2(imag, real);

				/* compute phase difference */
				tmp = phase - gLastPhase[k];
				gLastPhase[k] = (float)phase;

				/* subtract expected phase difference */
				tmp -= (double)k * expct;

				/* map delta phase into +/- Pi interval */
				qpd = (long)(tmp / M_PI);
				if (qpd >= 0) qpd += qpd & 1;
				else qpd -= qpd & 1;
				tmp -= M_PI * (double)qpd;

				/* get deviation from bin frequency from the +/- Pi interval */
				tmp = osamp * tmp / (2.0 * M_PI);

				/* compute the k-th partials' true frequency */
				tmp = (double)k * freqPerBin + tmp * freqPerBin;

				/* store magnitude and true frequency in analysis arrays */
				gAnaMagn[k] = (float)magn;
				gAnaFreq[k] = (float)tmp;

			}

			/* ***************** PROCESSING ******************* */
			/* this does the actual pitch shifting */
			for (int zero = 0; zero < fftFrameSize; zero++)
			{
				gSynMagn[zero] = 0;
				gSynFreq[zero] = 0;
			}

			for (k = 0; k <= fftFrameSize2; k++)
			{
				index = (long)(k * pitchShift);
				if (index <= fftFrameSize2)
				{
					gSynMagn[index] += gAnaMagn[k];
					gSynFreq[index] = gAnaFreq[k] * pitchShift;
				}
			}

			/* ***************** SYNTHESIS ******************* */
			/* this is the synthesis step */
			for (k = 0; k <= fftFrameSize2; k++)
			{

				/* get magnitude and true frequency from synthesis arrays */
				magn = gSynMagn[k];
				tmp = gSynFreq[k];

				/* subtract bin mid frequency */
				tmp -= (double)k * freqPerBin;

				/* get bin deviation from freq deviation */
				tmp /= freqPerBin;

				/* take osamp into account */
				tmp = 2.0 * M_PI * tmp / osamp;

				/* add the overlap phase advance back in */
				tmp += (double)k * expct;

				/* accumulate delta phase to get bin phase */
				gSumPhase[k] += (float)tmp;
				phase = gSumPhase[k];

				/* get real and imag part and re-interleave */
				gFFTworksp[2 * k] = (float)(magn * cos(phase));
				gFFTworksp[2 * k + 1] = (float)(magn * sin(phase));
			}

			/* zero negative frequencies */
			for (k = fftFrameSize + 2; k < 2 * fftFrameSize; k++) gFFTworksp[k] = 0.0F;

			/* do inverse transform */
			ShortTimeFourierTransform(gFFTworksp, fftFrameSize, 1);

			/* do windowing and add to output accumulator */
			for (k = 0; k < fftFrameSize; k++)
			{
				window = -.5 * cos(2.0 * M_PI * (double)k / (double)fftFrameSize) + .5;
				gOutputAccum[k] += (float)(2.0 * window * gFFTworksp[2 * k] / (fftFrameSize2 * osamp));
			}
			for (k = 0; k < stepSize; k++) gOutFIFO[k] = gOutputAccum[k];

			/* shift accumulator */
			//memmove(gOutputAccum, gOutputAccum + stepSize, fftFrameSize * sizeof(float));
			for (k = 0; k < fftFrameSize; k++)
			{
				gOutputAccum[k] = gOutputAccum[k + stepSize];
			}

			/* move input FIFO */
			for (k = 0; k < inFifoLatency; k++) gInFIFO[k] = gInFIFO[k + stepSize];
		}
	}
	return outdata;
}

std::vector<float> DoPitchShift(float pitchShift, long numSampsToProcess, float sampleRate, std::vector<float> indata)
{
	return DoPitchShift(pitchShift, numSampsToProcess, 2048, 10, sampleRate, indata);
}

PitchShift::PitchShift()
{
}

PitchShift::~PitchShift()
{
}

void PitchShift::process(int left, int right)
{
	int sample = (left + right) / 2;

	floatBuffer.push_back((float)sample);
}

void PitchShift::onWrite()
{
	std::cout << "Postprocessing..." << std::endl;

	std::vector<float> outFloat = DoPitchShift(1.25, floatBuffer.size(), sampleRate, floatBuffer);

	std::cout << "Copying data..." << std::endl;

	for (auto sample : outFloat)
	{
		int intSample = (int)sample;
		writeToBuffer(intSample, intSample);
	}
}