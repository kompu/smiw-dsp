#include "SecondPhaserEffect.h"

//#define M_PI 3.141592653589793
//#define STAGES_COUNT 8
//#define SWEEP 0.2
//#define WIDTH 0.8
//#define DEPTH 0.0
//#define FEEDBACK 0.0
//#define BOTTOM_FREQ 100.0
//#define SAMPLE_RATE sampleRate
//
//double sweepRate;
//double range;
//double wp, minwp, maxwp;
//double sweepFactor;
//
//struct AllpassFilter
//{
//	double out, in;
//};
//
//struct AllpassFilter filters[STAGES_COUNT];
//
//void init(struct AllpassFilter* filter)
//{
//	filter->in = 0.0;
//	filter->out = 0.0;
//}
//
//double filter(struct AllpassFilter* filter, double a, double sample)
//{
//	filter->out = a * (sample + filter->out) - filter->in;
//	filter->in = sample;
//	return filter->out;
//}
//
//double processSample(double sample)
//{
//	int i;
//	double a = (1.0 - wp) / (1.0 + wp);
//	double val = sample + FEEDBACK * filters[STAGES_COUNT - 1].out;
//
//	for (i = 0; i < STAGES_COUNT; ++i)
//	{
//		val = filter(&filters[i], a, val);
//	}
//
//	wp *= sweepFactor;
//	if (wp > maxwp || wp < minwp)
//	{
//		sweepFactor = 1.0 / sweepFactor;	// reverse
//	}
//
//	return sample + val * DEPTH;
//}


SecondPhaserEffect::SecondPhaserEffect()
{
}


SecondPhaserEffect::~SecondPhaserEffect()
{
}

void SecondPhaserEffect::onPrepare()
{
	/*sweepRate = pow(10.0, SWEEP);
	sweepRate -= 1.0;
	sweepRate *= 1.1;
	sweepRate += 0.1;

	range = 6.0 * WIDTH;

	wp = minwp = (M_PI * BOTTOM_FREQ) / SAMPLE_RATE;
	maxwp = minwp * range;

	sweepFactor = pow(range, sweepRate / (SAMPLE_RATE / 2.0));

	int i;
	for (i = 0; i < STAGES_COUNT; ++i)
	{
		init(&filters[i]);
	}*/
}

void SecondPhaserEffect::process(int left, int right)
{
	int sample = (left + right) / 2;
	//int sample = left;

	//sample = (int)(processSample(sample));

	writeToBuffer(sample, sample);
}